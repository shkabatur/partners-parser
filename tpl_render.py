from jinja2 import Environment, PackageLoader, select_autoescape

env = Environment(
    loader=PackageLoader("partners"),
    autoescape=select_autoescape()
)

template = env.get_template("name.tpl")
print(template.render({'my_name': 'Denis'}))