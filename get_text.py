import os
import re
from os.path import basename
from docx import Document


file_list = []
for subdir, dirs, files in os.walk('./'):
    for f in files:
        if f.endswith('.docx'):
            file_list.append(f)

file_list.sort()


document = Document(file_list[0])

# logo_url               URL логотипа                       
# partner_title          Название организации               3
# ruk                    Руководитель (ФИО)                 6
# address                Адрес организации                  9
# phone                  Телефон                            12
# email                  Почта                              14
# site_url               Сайт                               16
# manager_name           Менеджер проекта                   20
# manager_email          Почта менеджера                    24
# manager_phone          Телефон менеджера                  22
# about_desk             Описание конкурса                  26

lines = []
for table in document.tables:
    for row in table.rows:
      for cell in row.cells:
        print(dir(cell))
        print(cell.text)
        print("---------------------------")
