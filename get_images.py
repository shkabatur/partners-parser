import os
import re
from os.path import basename
from docx import Document


file_list = []
for subdir, dirs, files in os.walk('./'):
    for f in files:
        if f.endswith('.docx'):
            file_list.append(f)


#for table in document.tables:
#    for row in table.rows:
#      for cell in row.cells:
#        for para in cell.paragraphs:
#          print(para.text)
#
#for image in document.inline_shapes:
#    dir(image)
#    print (image.width, image.height)
#

def save_img(doc_name, doc_number):
    document = Document(doc_name)
    for shape in document.inline_shapes:
        contentID = shape._inline.graphic.graphicData.pic.blipFill.blip.embed
        contentType = document.part.related_parts[contentID].content_type
        if not contentType.startswith('image'):
            continue
        split_doc_name = re.split(',| ', doc_name)
        imgName = "imgs/" + split_doc_name[0] + "_" + basename(document.part.related_parts[contentID].partname)
        imgData = document.part.related_parts[contentID]._blob
        with open(imgName, 'wb') as fp:
            fp.write(imgData)


for i,f in enumerate(file_list):
    save_img(f,i+1)
